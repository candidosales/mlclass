# define function computeCost
computeCost = function(X, y, theta) { sum((X %*% theta - y) ^ 2) / (2 * nrow(X))}

# define function to compute next value of theta
nextTheta = function(X, y, theta, alpha) { theta - (t(X) %*% (X %*% theta - y) * alpha / nrow(y))}

# define function to perform gradientDescent
gradientDescent = function(X,y,theta,alpha,num_iters) {
    jHistory = rep(NA,num_iters);
    for (i in 1:num_iters) {
        theta = nextTheta(X, y, theta, alpha);
        jHistory[i] = computeCost(X, y, theta);
    }
    list(theta=theta, jHistory=jHistory);
}

# Read File
data = read.csv("ex1data1.txt",header=FALSE);

X = data.matrix(data[1])
y = data.matrix(data[2])

# Get #rows of data
m = nrow(data)

# Plot data
plot(data$V1, data$V2,xlab="Population of City in 10,000s", ylab="Profit in $10,000s",col="dark red",pch="X");

# Prepend X with a column of 1's
X = cbind(matrix(1,m,1),X)
colnames(X)[1] = "constant"
colnames(X)[2] = "population"

theta = matrix(0,2,1)
iterations = 1500
alpha = 0.01

result= gradientDescent(X,y,theta,alpha,1500);
theta = result$theta
jHistory = result$jHistory


plot(X[,2], y,xlab="Population of City in 10,000s", ylab="Profit in $10,000s",col="dark red",pch="X");
lines(X[,2],X %*% theta, type="l")

predict1 = matrix(c(1,35000),1,2) %*% theta
predict2 = matrix(c(1,75000),1,2) %*% theta

print(predict1)
print(predict2)

coster = function(t0, t1) { computeCost(X,y,matrix(c(t0,t1),2,1)) }
for (i in theta0_vals) {
    for(j in theta1_vals) {
        vals[i,j] = coster(i,j)
    }
}

